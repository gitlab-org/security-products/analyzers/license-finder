# frozen_string_literal: true

name "asdf_maven"
default_version "3.6.3"

major = version.split('.')[0]
source url: "https://archive.apache.org/dist/maven/maven-#{major}/#{version}/binaries/apache-maven-#{version}-bin.tar.gz"
relative_path "apache-maven-#{version}"

version "3.6.3" do
  source sha256: "26ad91d751b3a9a53087aefa743f4e16a17741d3915b219cf74112bf87a438c5"
end

build do
  mkdir install_dir
  copy "#{project_dir}/**", "#{install_dir}/"
end
