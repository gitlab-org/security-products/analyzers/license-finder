# frozen_string_literal: true

name "asdf_mono"
default_version "6.8.0.123"
license_file "LICENSE"
license_file "COPYING"

dependency "zlib"

source url: "https://download.mono-project.com/sources/mono/mono-#{version}.tar.xz"
relative_path "mono-#{version}"

version("6.8.0.123") do
  source sha256: 'e2e42d36e19f083fc0d82f6c02f7db80611d69767112af353df2f279744a2ac5'
end

build do
  env = with_standard_compiler_flags(with_embedded_path)
  configure_command = [
    "--disable-crash-reporting",
    # "--enable-minimal=profiler,decimal,pinvoke,debug,appdomains,verifier,reflection_emit,reflection_emit_save,large_code,logging,com,ssa,generics,attach,interpreter,simd,soft_debug,perfcounters,normalization,desktop_loader,shared_perfcounters,remoting,security,lldb,mdb,assert_messages,cleanup,sgen_marksweep_conc,sgen_split_nursery,sgen_gc_bridge,sgen_debug_helpers,sockets,gac",
    "--enable-small-config",
    "--with-crash-privacy=yes",
    "--with-libgdiplus=sibling",
    "--with-moonlight=no",
    "--with-x=no",
    "--with-mcs-docs=no",
    "--prefix=#{install_dir}"
  ]
  env['CFLAGS'] << ' -Os'

  configure(*configure_command, env: env)
  make "-j #{workers}", env: env
  make "-j #{workers} install", env: env
end

build do
  command "curl -o #{install_dir}/bin/nuget.exe https://dist.nuget.org/win-x86-commandline/latest/nuget.exe"
  copy "#{install_dir}/bin/nuget.exe", "#{install_dir}/bin/nuget"
end
