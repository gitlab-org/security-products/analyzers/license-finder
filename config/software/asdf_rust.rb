# frozen_string_literal: true

name "asdf_rust"
default_version "1.45.0"
whitelist_file "lib/rustlib/x86_64-unknown-linux-gnu/bin/rust-lld"

source url: "https://static.rust-lang.org/dist/rust-#{version}-x86_64-unknown-linux-gnu.tar.gz"
relative_path "rust-#{version}-x86_64-unknown-linux-gnu"

version "1.45.0" do
  source sha256: "c34ed8722759fd60c94dbc9069833da5b3b873dcd19afaa9b34c1ce2c2cfa229"
end

build do
  env = with_standard_compiler_flags(with_embedded_path)
  command "sh install.sh" \
    " --prefix=#{install_dir}" \
    " --components=rustc,cargo" \
    " --verbose", env: env
end

build do
  delete "#{install_dir}/share"
  delete "#{install_dir}/etc"
  delete "#{install_dir}/bin/rustdoc"
  delete "#{install_dir}/bin/rust-gdb"
  delete "#{install_dir}/bin/rust-gdbgui"
end
