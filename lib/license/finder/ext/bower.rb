# frozen_string_literal: true

module LicenseFinder
  class Bower
    def possible_package_paths
      [project_path.join('bower.json')]
    end

    def prepare
      within_project_path do
        tool_box.install(tool: :nodejs, env: default_env)
        shell.execute([
          :bower,
          :install,
          '--allow-root',
          '--production',
          '--verbose',
          '--loglevel',
          :debug
        ], env: default_env, capture: false)
      end
    end

    def current_packages
      within_project_path do
        map_all(bower_output).flatten.compact
      end
    end

    private

    def bower_output
      stdout, _stderr, status = shell.execute([
        :bower, :list, '--json', '-l', 'action', '--allow-root'
      ], env: default_env)
      return {} unless status.success?

      JSON.parse(stdout)
    end

    def map_all(modules)
      [map_from(modules)] +
        modules.fetch('dependencies', {}).values.map { |x| map_all(x) }
    end

    def map_from(bower_module)
      meta = bower_module.fetch('pkgMeta', {})
      endpoint = bower_module.fetch('endpoint', {})

      Dependency.new(
        'Bower',
        meta['name'] || endpoint['name'],
        meta['version'] || endpoint['target'],
        description: meta['readme'],
        detection_path: detected_package_path,
        homepage: meta['homepage'],
        install_path: bower_module['canonicalDir'],
        spec_licenses: Package.license_names_from_standard_spec(meta),
        summary: meta['description']
      )
    end

    def default_env
      @default_env ||= {
        'NODE_EXTRA_CA_CERTS' => ENV.fetch('NODE_EXTRA_CA_CERTS', shell.default_certificate_path).to_s,
        'NPM_CONFIG_CAFILE' => ENV.fetch('NPM_CONFIG_CAFILE', shell.default_certificate_path).to_s,
        'bower_ca' => ENV.fetch('bower_ca', shell.default_certificate_path).to_s,
        'bower_directory' => ENV.fetch('bower_directory', vendor_path.join('bower_components')).to_s
      }
    end
  end
end
